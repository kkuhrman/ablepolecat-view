# AblePolecat-View
A simple MVC implementation using the AblePolecat core class library.

Creating a new Eclipse project by cloning an existing Git repository
1. Open Git Repository view in Eclipse
   Window | Show View | Other... | Git | Git Repositories
2. Click on Clone a Git Repository button and add the existing repository to the view
3. Create a new Eclipse project and select the "Create project at existing location (from existing source) and select the directory where the Git repository was cloned to in step 2

Creating a local web project by cloning AblePolecat-View Git repository in Eclipse:
1. Make sure your user is a member of webusers (or apache) group
2. Create directory for virtual host in /var/www e.g. /var/www/sites/AblePolecat-View
3. Change ownership of directory created in #2 to webuser:webusers (or apache:apache)
4. Add write privileges for group to directory created in #2 e.g. chmod g+w /var/www/sites/AblePolecat-View
5. Follow instructions above to clone AblePolecat-View repository under directory created in #2 (make sure to change name of working copy directory from default - e.g. 'master' - to something meaningful as this will show in Git repositories view in Eclipse e.g. AblePolecat-View); include step #3 above and create the PHP project off the working copy source code
6. clone AblePolecat Git repository in [path/to/working/copy/created/in/5]/usr/lib/AblePolecat (similar to step #5); include step #3 and create a separate Eclipse project off the working copy of the AblePolecat repository
7. repeat for other AblePolecat module projects